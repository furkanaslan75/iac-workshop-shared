# Create an Azure Container Registry to build and store the Docker API

## Steps

1. Open a command prompt in the folder containing the code
1. Verify your active Azure subscription with `az account show` or
1. Log in using azure cli (`az login` if needed with `-t <tenantid>`) and select the correct subscription: `az account set --subscription="<subscription guid or name>"`
1. Initialize Terraform: `terraform init`
1. Execute a Terraform Plan and save the plan with `terraform plan -out tfplan`
1. When asked set the variable `randomstring` to something unique and max 8 characters long
1. Review the plan. If all looks OK run `terraform apply tfplan` and wait until the ACR is created.
1. Take a note of the URL of the ACR for the next assignment (_xxxx.azurecr.io_). It is in the Terraform output.
