variable "environment" {
  description = "Environment"
  type        = string
  default     = "shared"
}

variable "location" {
  description = "Azure Location"
  type        = string
  default     = "westeurope"
}

variable "randomstring" {
  description = "Random string for globally unique resources like storage accounts"
  type        = string
}
